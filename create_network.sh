#!/bin/bash
# Create docker network cbionetwokr if does not exist. 

NETWORKNAME="cbionetwork"
NETWORKSTATUS=$(docker network ls | grep $NETWORKNAME |wc -l)

echo $NETWORKSTATUS
if [ ! $NETWORKSTATUS -eq 0 ]; then
    print "Network already exists."
else
    print "Creating network."
    docker network create -d bridge $NETWORKNAME
fi
